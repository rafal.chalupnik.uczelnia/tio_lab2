﻿namespace GeneticAlgorithm.Core.Genotypes
{
    public interface IGenotypeFactory
    {
        Genotype CreateGenotype();
    }
}