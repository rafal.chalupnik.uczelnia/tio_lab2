﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Evaluator;

namespace GeneticAlgorithm.Core.Genotypes
{
    public class Genotype
    {
        private readonly IEvaluator evaluator;
        private readonly Random random;

        private readonly int[] genotype;

        public Genotype(Random random, IEvaluator evaluator, int genotypeSize)
        {
            this.random = random;
            this.evaluator = evaluator;

            genotype = new int[genotypeSize];
        }

        public int this[int index]
        {
            get => genotype[index];
            set => genotype[index] = value;
        }

        public int EvaluationResult => evaluator.Evaluate(genotype);

        public bool Contains(int value)
        {
            return genotype.ToList().Contains(value);
        }

        public int IndexOf(int value)
        {
            return genotype.ToList().IndexOf(value);
        }

        public void RepairIfNeeded()
        {
            if (!Validate())
            {
                Repair();
            }
        }

        public override string ToString()
        {
            return string.Join(", ", genotype);
        }

        private bool Validate()
        {
            var listSolution = genotype.ToList();

            if (listSolution.Min() < 0 || listSolution.Max() >= listSolution.Count)
                return false;

            if (listSolution.Count != listSolution.Distinct().Count())
                return false;

            return true;
        }

        private void Repair()
        {
            var notOccured = Enumerable.Range(0, genotype.Length).ToList();
            var alreadyOccured = new List<int>();

            for (var i = 0; i < genotype.Length; i++)
            {
                if (alreadyOccured.Contains(genotype[i]))
                    genotype[i] = notOccured[random.Next(0, notOccured.Count)];

                alreadyOccured.Add(genotype[i]);
                notOccured.Remove(genotype[i]);
            }
        }
    }
}