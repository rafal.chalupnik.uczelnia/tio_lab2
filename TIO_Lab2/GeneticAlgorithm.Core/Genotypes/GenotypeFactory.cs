﻿using System;
using GeneticAlgorithm.Core.Evaluator;

namespace GeneticAlgorithm.Core.Genotypes
{
    public class GenotypeFactory : IGenotypeFactory
    {
        private readonly int genotypeSize;
        private readonly Random random;
        private readonly IEvaluator evaluator;

        public GenotypeFactory(int genotypeSize, Random random, IEvaluator evaluator)
        {
            this.genotypeSize = genotypeSize;
            this.random = random;
            this.evaluator = evaluator;
        }

        public Genotype CreateGenotype()
        {
            return new Genotype(random, evaluator, genotypeSize);
        }
    }
}