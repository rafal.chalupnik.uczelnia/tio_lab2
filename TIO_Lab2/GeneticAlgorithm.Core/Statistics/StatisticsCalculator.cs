﻿using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Statistics
{
    public class StatisticsCalculator : IStatisticsCalculator
    {
        public Statistics CalculateStatistics(IReadOnlyCollection<Genotype> population)
        {
            var results = population
                .Select(genotype => genotype.EvaluationResult)
                .ToList();

            var bestResult = results.Min();
            var worstResult = results.Max();
            var averageResult = results.Average();
            var bestGenotype = population.First(genotype => genotype.EvaluationResult == bestResult);

            return new Statistics(bestGenotype, bestResult, averageResult, worstResult, population.Count);
        }
    }
}