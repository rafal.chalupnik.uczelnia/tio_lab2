﻿using System.Collections.Generic;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Statistics
{
    public interface IStatisticsCalculator
    {
        Statistics CalculateStatistics(IReadOnlyCollection<Genotype> population);
    }
}