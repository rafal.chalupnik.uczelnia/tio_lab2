﻿using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Statistics
{
    public class Statistics
    {
        public int AverageScore { get; }
        public int BestScore { get; }
        public Genotype BestSolution { get; }
        public int Population { get; }
        public int WorseScore { get; }

        public Statistics(Genotype bestSolution, int bestScore, double averageScore, int worseScore, int population)
        {
            BestSolution = bestSolution;
            BestScore = bestScore;
            AverageScore = (int) averageScore;
            WorseScore = worseScore;
            Population = population;
        }
    }
}