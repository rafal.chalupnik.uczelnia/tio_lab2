﻿using GeneticAlgorithm.Core.Evaluator;
using Lab2;
using System;
using System.Collections.Generic;
using GeneticAlgorithm.Core.Crossovers;
using GeneticAlgorithm.Core.Genotypes;
using GeneticAlgorithm.Core.Initializers;
using GeneticAlgorithm.Core.Mutators;
using GeneticAlgorithm.Core.Selectors;
using GeneticAlgorithm.Core.Statistics;

namespace GeneticAlgorithm.Core
{
    public class GeneticAlgorithm
    {
        private readonly ICrossover crossover;
        private readonly IEvaluator evaluator;
        private readonly ILogger logger;
        private readonly IMutator mutator;
        private readonly ISelector selector;
        private readonly IStatisticsCalculator statisticsCalculator;
        
        private readonly List<List<Genotype>> populationHistory;
        private List<Genotype> population;

        public GeneticAlgorithm(IEvaluator evaluator, ILogger logger, IPopulationInitializer populationInitializer, ISelector selector, ICrossover crossover, IMutator mutator, IStatisticsCalculator statisticsCalculator)
        {
            this.evaluator = evaluator;
            this.logger = logger;
            this.selector = selector;
            this.crossover = crossover;
            this.mutator = mutator;
            this.statisticsCalculator = statisticsCalculator;

            population = populationInitializer.Initialize();
            populationHistory = new List<List<Genotype>>();
        }

        public Result Run(Func<int, List<Genotype>, bool> stopCondition)
        {
            var iteration = 0;
            var statistics = statisticsCalculator.CalculateStatistics(population);
            logger.LogIteration(iteration, statistics);

            while (!stopCondition(iteration, population))
            {
                iteration++;

                var parents = selector.SelectParents(population);
                var newPopulation = crossover.Crossover(parents);

                foreach (var genotype in newPopulation)
                {
                    mutator.Mutate(genotype);
                }

                newPopulation.Add(statistics.BestSolution);

                statistics = statisticsCalculator.CalculateStatistics(newPopulation);
                logger.LogIteration(iteration, statistics);
                
                populationHistory.Add(population);
                population = newPopulation;
            }

            return new Result(statistics.BestSolution, statistics.BestScore, evaluator.Counter);
        }
    }
}