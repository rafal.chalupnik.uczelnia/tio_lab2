﻿using System;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Mutators.Implementations
{
    public class SwapMutator : IMutator
    {
        private readonly int genotypeSize;
        private readonly double mutationChance;
        private readonly Random random;

        public SwapMutator(Random random, double mutationChance, int genotypeSize)
        {
            this.genotypeSize = genotypeSize;
            this.mutationChance = mutationChance;
            this.random = random;
        }

        public void Mutate(Genotype genotype)
        {
            if (!ShouldMutate())
                return;

            var indexes = TakeRandomIndexes();
            Swap(genotype, indexes);
        }

        private bool ShouldMutate()
        {
            return random.NextDouble() <= mutationChance;
        }

        private Tuple<int, int> TakeRandomIndexes()
        {
            var indexes = Enumerable.Range(0, genotypeSize).ToList();
            var index1 = indexes[random.Next(indexes.Count)];
            indexes.Remove(index1);
            var index2 = indexes[random.Next(indexes.Count)];

            return Tuple.Create(index1, index2);
        }

        private static void Swap(Genotype genotype, Tuple<int, int> indexes)
        {
            var temp = genotype[indexes.Item1];
            genotype[indexes.Item1] = genotype[indexes.Item2];
            genotype[indexes.Item2] = temp;
        }
    }
}