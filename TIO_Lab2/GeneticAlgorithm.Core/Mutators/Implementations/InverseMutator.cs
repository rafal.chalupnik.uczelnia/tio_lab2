﻿using System;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Mutators.Implementations
{
    public class InverseMutator : IMutator
    {
        private readonly int genotypeSize;
        private readonly double mutationChance;
        private readonly Random random;

        public InverseMutator(int genotypeSize, double mutationChance, Random random)
        {
            this.genotypeSize = genotypeSize;
            this.mutationChance = mutationChance;
            this.random = random;
        }

        public void Mutate(Genotype genotype)
        {
            if (ShouldMutate())
                Inverse(genotype);
        }

        private bool ShouldMutate()
        {
            return random.NextDouble() <= mutationChance;
        }

        private void Inverse(Genotype genotype)
        {
            var (firstPoint, secondPoint) = GetRandomCrossPoints();

            while (firstPoint < secondPoint)
            {
                Swap(genotype, Tuple.Create(firstPoint, secondPoint));
                firstPoint++;
                secondPoint--;
            }
        }

        private (int firstPoint, int secondPoint) GetRandomCrossPoints()
        {
            var firstPoint = random.Next(genotypeSize);
            var secondPoint = random.Next(genotypeSize);

            return firstPoint <= secondPoint
                ? (firstPoint, secondPoint)
                : (secondPoint, firstPoint);
        }

        private static void Swap(Genotype genotype, Tuple<int, int> indexes)
        {
            var temp = genotype[indexes.Item1];
            genotype[indexes.Item1] = genotype[indexes.Item2];
            genotype[indexes.Item2] = temp;
        }
    }
}