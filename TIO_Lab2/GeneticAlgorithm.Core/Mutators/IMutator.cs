﻿using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Mutators
{
    public interface IMutator
    {
        void Mutate(Genotype genotype);
    }
}