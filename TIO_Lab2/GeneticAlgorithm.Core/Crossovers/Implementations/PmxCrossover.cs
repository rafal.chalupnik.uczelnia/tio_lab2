﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Crossovers.Implementations
{
    public class PmxCrossover : ICrossover
    {
        private readonly double crossoverChance;
        private readonly int genotypeSize;
        private readonly IGenotypeFactory genotypeFactory;
        private readonly Random random;

        public PmxCrossover(double crossoverChance, int genotypeSize, IGenotypeFactory genotypeFactory, Random random)
        {
            this.crossoverChance = crossoverChance;
            this.genotypeSize = genotypeSize;
            this.genotypeFactory = genotypeFactory;
            this.random = random;
        }

        public List<Genotype> Crossover(List<Tuple<Genotype, Genotype>> parentsCouples)
        {
            return parentsCouples
                .SelectMany(ProcessCouple)
                .ToList();
        }

        private IEnumerable<Genotype> ProcessCouple(Tuple<Genotype, Genotype> parentsCouple)
        {
            return TakeCrossoverChance()
                ? CrossoverParents(parentsCouple.Item1, parentsCouple.Item2)
                : ReturnParents(parentsCouple);
        }

        private bool TakeCrossoverChance()
        {
            return random.NextDouble() <= crossoverChance;
        }

        private IEnumerable<Genotype> CrossoverParents(Genotype parent1, Genotype parent2)
        {
            var child1 = genotypeFactory.CreateGenotype();
            var child2 = genotypeFactory.CreateGenotype();

            for (var i = 0; i < genotypeSize; i++)
            {
                child1[i] = -1;
                child2[i] = -1;
            }

            var (firstCrossPoint, secondCrossPoint) = GetRandomCrossPoints();

            for (var i = firstCrossPoint; i < secondCrossPoint; i++)
            {
                child1[i] = parent1[i];
                child2[i] = parent2[i];
            }

            for (var i = 0; i < firstCrossPoint; i++)
            {
                var item1 = parent2[i];

                var itemIndex1 = child1.IndexOf(item1);

                while (itemIndex1 != -1)
                {
                    item1 = parent2[itemIndex1];
                    itemIndex1 = child1.IndexOf(item1);
                }

                child1[i] = item1;

                var item2 = parent1[i];

                var itemIndex2 = child2.IndexOf(item2);

                while (itemIndex2 != -1)
                {
                    item2 = parent1[itemIndex2];
                    itemIndex2 = child2.IndexOf(item2);
                }

                child2[i] = item2;
            }

            for (var i = secondCrossPoint; i < genotypeSize; i++)
            {
                var item1 = parent2[i];

                var itemIndex1 = child1.IndexOf(item1);

                while (itemIndex1 != -1)
                {
                    item1 = parent2[itemIndex1];
                    itemIndex1 = child1.IndexOf(item1);
                }

                child1[i] = item1;

                var item2 = parent1[i];

                var itemIndex2 = child2.IndexOf(item2);

                while (itemIndex2 != -1)
                {
                    item2 = parent1[itemIndex2];
                    itemIndex2 = child2.IndexOf(item2);
                }

                child2[i] = item2;
            }

            return new List<Genotype>
            {
                child1,
                child2
            };
        }

        private (int firstCrossPoint, int secondCrossPoint) GetRandomCrossPoints()
        {
            var firstCrossPoint = random.Next(genotypeSize);
            var secondCrossPoint = random.Next(genotypeSize);

            return firstCrossPoint <= secondCrossPoint
                ? (firstCrossPoint, secondCrossPoint)
                : (secondCrossPoint, firstCrossPoint);
        }

        private static IEnumerable<Genotype> ReturnParents(Tuple<Genotype, Genotype> parents)
        {
            return new List<Genotype>
            {
                parents.Item1,
                parents.Item2
            };
        }
    }
}