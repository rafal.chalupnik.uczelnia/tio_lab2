﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Crossovers.Implementations
{
    public class XorCrossover : ICrossover
    {
        private readonly double crossoverChance;
        private readonly IGenotypeFactory genotypeFactory;
        private readonly int genotypeSize;
        private readonly Random random;

        public XorCrossover(int genotypeSize, double crossoverChance, Random random, IGenotypeFactory genotypeFactory)
        {
            this.genotypeSize = genotypeSize;
            this.crossoverChance = crossoverChance;
            this.random = random;
            this.genotypeFactory = genotypeFactory;
        }

        public List<Genotype> Crossover(List<Tuple<Genotype, Genotype>> parentsCouples)
        {
            return parentsCouples
                .SelectMany(ProcessParentsCouple)
                .ToList();
        }

        private IEnumerable<Genotype> ProcessParentsCouple(Tuple<Genotype, Genotype> parentsCouple)
        {
            return TakeCrossoverChance()
                ? CrossoverParents(parentsCouple.Item1, parentsCouple.Item2)
                : ReturnParents(parentsCouple);
        }

        private bool TakeCrossoverChance()
        {
            return random.NextDouble() >= crossoverChance;
        }

        private IEnumerable<Genotype> CrossoverParents(Genotype parent1, Genotype parent2)
        {
            var child1 = genotypeFactory.CreateGenotype();
            var child2 = genotypeFactory.CreateGenotype();

            for (var point = 0; point < genotypeSize; point++)
            {
                child1[point] = parent1[point] ^ parent2[point];
                child2[point] = ~(parent1[point] ^ parent2[point]);
            }

            child1.RepairIfNeeded();
            child2.RepairIfNeeded();

            return new List<Genotype>
            {
                child1,
                child2
            };
        }

        private static IEnumerable<Genotype> ReturnParents(Tuple<Genotype, Genotype> parents)
        {
            return new List<Genotype>
            {
                parents.Item1,
                parents.Item2
            };
        }
    }
}