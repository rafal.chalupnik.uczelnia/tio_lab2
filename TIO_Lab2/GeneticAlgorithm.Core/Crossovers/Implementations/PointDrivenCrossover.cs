﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Crossovers.Implementations
{
    public class PointDrivenCrossover : ICrossover
    {
        private readonly double crossoverChance;
        private readonly List<int> crossPoints;
        private readonly IGenotypeFactory genotypeFactory;
        private readonly int genotypeSize;
        private readonly Random random;

        public PointDrivenCrossover(int genotypeSize, IGenotypeFactory genotypeFactory, List<int> crossPoints, double crossoverChance, Random random)
        {
            this.genotypeSize = genotypeSize;
            this.genotypeFactory = genotypeFactory;
            this.crossPoints = crossPoints;
            this.crossoverChance = crossoverChance;
            this.random = random;
        }

        public List<Genotype> Crossover(List<Tuple<Genotype, Genotype>> parentsCouples)
        {
            return parentsCouples
                .SelectMany(ProcessCouple)
                .ToList();
        }

        private IEnumerable<Genotype> ProcessCouple(Tuple<Genotype, Genotype> parentsCouple)
        {
            return TakeCrossoverChance()
                ? CrossoverParents(parentsCouple.Item1, parentsCouple.Item2)
                : ReturnParents(parentsCouple);
        }

        private bool TakeCrossoverChance()
        {
            return random.NextDouble() >= crossoverChance;
        }

        private IEnumerable<Genotype> CrossoverParents(Genotype parent1, Genotype parent2)
        {
            var child1 = genotypeFactory.CreateGenotype();
            var child2 = genotypeFactory.CreateGenotype();

            var inverted = false;

            for (var point = 0; point < genotypeSize; point++)
            {
                if (crossPoints.Contains(point))
                    inverted = !inverted;

                child1[point] = inverted
                    ? parent2[point]
                    : parent1[point];

                child2[point] = inverted
                    ? parent1[point]
                    : parent2[point];
            }

            child1.RepairIfNeeded();
            child2.RepairIfNeeded();

            return new List<Genotype>
            {
                child1,
                child2
            };
        }

        private static IEnumerable<Genotype> ReturnParents(Tuple<Genotype, Genotype> parents)
        {
            return new List<Genotype>
            {
                parents.Item1,
                parents.Item2
            };
        }
    }
}