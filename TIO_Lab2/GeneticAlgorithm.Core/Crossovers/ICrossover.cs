﻿using System;
using System.Collections.Generic;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Crossovers
{
    public interface ICrossover
    {
        List<Genotype> Crossover(List<Tuple<Genotype, Genotype>> parentsCouples);
    }
}