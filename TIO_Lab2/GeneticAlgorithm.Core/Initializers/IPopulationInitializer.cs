﻿using System.Collections.Generic;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Initializers
{
    public interface IPopulationInitializer
    {
        List<Genotype> Initialize();
    }
}