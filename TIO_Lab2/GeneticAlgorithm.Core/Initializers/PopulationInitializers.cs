﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Initializers
{
    public class PopulationInitializer : IPopulationInitializer
    {
        private readonly IGenotypeFactory genotypeFactory;
        private readonly int genotypeSize;
        private readonly int initialPopulationSize;
        private readonly Random random;

        public PopulationInitializer(int genotypeSize, int initialPopulationSize, IGenotypeFactory genotypeFactory, Random random)
        {
            this.genotypeSize = genotypeSize;
            this.initialPopulationSize = initialPopulationSize;
            this.genotypeFactory = genotypeFactory;
            this.random = random;
        }

        public List<Genotype> Initialize()
        {
            return Enumerable.Range(0, initialPopulationSize)
                .Select(i => CreateRandomGenotype())
                .ToList();
        }

        private Genotype CreateRandomGenotype()
        {
            var available = Enumerable.Range(0, genotypeSize).ToList();

            var solution = genotypeFactory.CreateGenotype();
            for (var i = 0; i < genotypeSize; i++)
            {
                solution[i] = available[random.Next(available.Count)];
                available.Remove(solution[i]);
            }

            return solution;
        }
    }
}