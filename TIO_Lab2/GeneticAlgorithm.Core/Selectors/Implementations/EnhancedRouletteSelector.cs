﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;
using GeneticAlgorithm.Core.Statistics;
using Lab2;

namespace GeneticAlgorithm.Core.Selectors.Implementations
{
    public class EnhancedRouletteSelector : RouletteBase, ISelector
    {
        private readonly IStatisticsCalculator statisticsCalculator;

        public EnhancedRouletteSelector(int initialPopulationSize, Random random, IStatisticsCalculator statisticsCalculator) : base(initialPopulationSize, random)
        {
            this.statisticsCalculator = statisticsCalculator;
        }

        public List<Tuple<Genotype, Genotype>> SelectParents(IReadOnlyCollection<Genotype> population)
        {
            var roulette = CreateRoulette(population);
            return SelectParentsFromRoulette(roulette);
        }

        private List<Genotype> CreateRoulette(IReadOnlyCollection<Genotype> population)
        {
            var statistics = statisticsCalculator.CalculateStatistics(population);

            return population
                .SelectMany(genotype => CreateGenotypeTickets(genotype, statistics))
                .ToList();
        }

        private static IEnumerable<Genotype> CreateGenotypeTickets(Genotype genotype, Statistics.Statistics statistics)
        {
            var fitness = statistics.WorseScore - genotype.EvaluationResult;

            return Enumerable.Range(0, fitness)
                .Select(i => genotype)
                .ToList();
        }
    }
}