﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Selectors.Implementations
{
    public class TournamentSelector : ISelector
    {
        private readonly int initialPopulationSize;
        private readonly Random random;
        private readonly int tournamentSize;

        public TournamentSelector(int initialPopulationSize, Random random, int tournamentSize)
        {
            this.initialPopulationSize = initialPopulationSize;
            this.random = random;
            this.tournamentSize = tournamentSize;
        }

        public List<Tuple<Genotype, Genotype>> SelectParents(IReadOnlyCollection<Genotype> population)
        {
            var parents = new List<Tuple<Genotype, Genotype>>();

            for (var j = 0; j < initialPopulationSize / 2; j++)
            {
                var parent1 = GetGenotypeFromTournament(population);
                var parent2 = GetGenotypeFromTournament(population);

                while (parent1 == parent2)
                {
                    parent2 = GetGenotypeFromTournament(population);
                }

                parents.Add(Tuple.Create(parent1, parent2));
            }

            return parents;
        }
        
        private Genotype GetGenotypeFromTournament(IReadOnlyCollection<Genotype> population)
        {
            var tournament = population.TakeDistinctRandom(tournamentSize, random);
            var bestScore = tournament
                .Select(genotype => genotype.EvaluationResult)
                .Min();

            return tournament.First(genotype => genotype.EvaluationResult == bestScore);
        }
    }
}