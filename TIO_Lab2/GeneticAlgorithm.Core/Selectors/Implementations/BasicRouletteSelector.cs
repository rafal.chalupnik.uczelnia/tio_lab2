﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Selectors.Implementations
{
    public class BasicRouletteSelector : RouletteBase, ISelector
    {
        public BasicRouletteSelector(int initialPopulationSize, Random random) : base(initialPopulationSize, random) { }

        public List<Tuple<Genotype, Genotype>> SelectParents(IReadOnlyCollection<Genotype> population)
        {
            var roulette = CreateRoulette(population);
            return SelectParentsFromRoulette(roulette);
        }

        private static List<Genotype> CreateRoulette(IEnumerable<Genotype> population)
        {
            return population
                .SelectMany(CreateGenotypeTickets)
                .ToList();
        }

        private static List<Genotype> CreateGenotypeTickets(Genotype genotype)
        {
            var fitness = (int)(1.0 / genotype.EvaluationResult * 10000);

            return Enumerable.Range(0, fitness)
                .Select(i => genotype)
                .ToList();
        }
    }
}