﻿using System;
using System.Collections.Generic;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Selectors
{
    public interface ISelector
    {
        List<Tuple<Genotype, Genotype>> SelectParents(IReadOnlyCollection<Genotype> population);
    }
}