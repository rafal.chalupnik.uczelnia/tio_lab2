﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.Core.Genotypes;

namespace GeneticAlgorithm.Core.Selectors
{
    public abstract class RouletteBase
    {
        private readonly int initialPopulationSize;
        private readonly Random random;

        protected RouletteBase(int initialPopulationSize, Random random)
        {
            this.initialPopulationSize = initialPopulationSize;
            this.random = random;
        }

        protected List<Tuple<Genotype, Genotype>> SelectParentsFromRoulette(IReadOnlyCollection<Genotype> roulette)
        {
            return Enumerable.Range(0, initialPopulationSize / 2)
                .Select(i => GetRandomParentsFromRoulette(roulette))
                .ToList();
        }

        private Tuple<Genotype, Genotype> GetRandomParentsFromRoulette(IReadOnlyCollection<Genotype> roulette)
        {
            var parents = roulette.TakeDistinctRandom(2, random);
            return Tuple.Create(
                parents.First(),
                parents.Last());
        }
    }

    public static class Extensions
    {
        public static List<T> TakeDistinctRandom<T>(this IReadOnlyCollection<T> list, int count, Random random = null)
        {
            if (list.Count < count)
                throw new InvalidOperationException("Collection is smaller than demanded count.");

            if (list.Count == count)
                return list.ToList();

            if (random == null)
                random = new Random();

            var available = list.ToList();
            var taken = new List<T>();

            while (taken.Count != count)
            {
                var element = available[random.Next(available.Count)];

                taken.Add(element);
                available.Remove(element);
            }

            return taken;
        }
    }
}