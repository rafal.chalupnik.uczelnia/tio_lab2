﻿using System;
using System.Collections.Generic;
using GeneticAlgorithm.Core.Crossovers;
using GeneticAlgorithm.Core.Crossovers.Implementations;
using GeneticAlgorithm.Core.Evaluator;
using GeneticAlgorithm.Core.Genotypes;
using GeneticAlgorithm.Core.Initializers;
using GeneticAlgorithm.Core.Mutators;
using GeneticAlgorithm.Core.Mutators.Implementations;
using GeneticAlgorithm.Core.Selectors;
using GeneticAlgorithm.Core.Selectors.Implementations;
using GeneticAlgorithm.Core.Statistics;
using Lab2;

namespace GeneticAlgorithm.CLI
{
    public static class Program
    {
        private const double CrossoverChance = 0.8;
        private const int GenotypeSize = 20;
        private const int InitialPopulationSize = 1000;
        private const int Iterations = 30;
        private const string LogFilePath = "combo.csv";
        private const double MutationChance = 0.2;
        private const int TournamentSize = 5;

        private static readonly List<int> CrossPoints = new List<int>
        {
            5,
            10,
            15
        };
        private static readonly string DataFilePath = $@"Data\had{GenotypeSize}.dat";
        private static readonly TimeSpan WorkTime = TimeSpan.FromMinutes(5);
        
        public static void Main(string[] args)
        {
            RunGeneticAlgorithm();
        }

        private static void RunGeneticAlgorithm()
        {
            var random = new Random();

            var evaluator = CreateEvaluator();
            var genotypeFactory = CreateGenotypeFactory(random, evaluator);
            var statisticsCalculator = CreateStatisticsCalculator();

            var logger = CreateComboLogger();
            var populationInitializer = CreatePopulationInitializer(genotypeFactory, random);
            var selector = CreateEnhancedRouletteSelector(random, statisticsCalculator);
            var crossover = CreatePmxCrossover(genotypeFactory, random);
            var mutator = CreateInverseMutator(random);
            
            var geneticAlgorithm = new Core.GeneticAlgorithm(
                evaluator, 
                logger, 
                populationInitializer, 
                selector,
                crossover, 
                mutator, 
                statisticsCalculator);

            var stopCondition = CreateTimeStopCondition();
            var result = geneticAlgorithm.Run(stopCondition);
            
            Console.WriteLine($"Result: {result}");

            logger.Close();
            Console.WriteLine("Done!");
            Console.ReadLine();
        }

        private static IEvaluator CreateEvaluator()
        {
            return EvaluatorLoader.LoadFromFile(DataFilePath);
        }

        private static ILogger CreateConsoleLogger()
        {
            return new ConsoleLogger();
        }

        private static ILogger CreateFileLogger()
        {
            return new FileLogger(LogFilePath);
        }

        private static ILogger CreateComboLogger()
        {
            return new ComboLogger(
                new FileLogger(LogFilePath), 
                new ConsoleLogger());
        }

        private static IPopulationInitializer CreatePopulationInitializer(IGenotypeFactory genotypeFactory, Random random)
        {
            return new PopulationInitializer(GenotypeSize, InitialPopulationSize, genotypeFactory, random);
        }

        private static ISelector CreateBasicRouletteSelector(Random random)
        {
            return new BasicRouletteSelector(InitialPopulationSize, random);
        }

        private static ISelector CreateEnhancedRouletteSelector(Random random, IStatisticsCalculator statisticsCalculator)
        {
            return new EnhancedRouletteSelector(InitialPopulationSize, random, statisticsCalculator);
        }

        private static ISelector CreateTournamentSelector(Random random)
        {
            return new TournamentSelector(InitialPopulationSize, random, TournamentSize);
        }

        private static ICrossover CreatePointDrivenCrossover(IGenotypeFactory genotypeFactory, Random random)
        {
            return new PointDrivenCrossover(GenotypeSize, genotypeFactory, CrossPoints, CrossoverChance, random);
        }

        private static ICrossover CreateXorCrossover(IGenotypeFactory genotypeFactory, Random random)
        {
            return new XorCrossover(GenotypeSize, CrossoverChance, random, genotypeFactory);
        }

        private static ICrossover CreatePmxCrossover(IGenotypeFactory genotypeFactory, Random random)
        {
            return new PmxCrossover(CrossoverChance, GenotypeSize, genotypeFactory, random);
        }

        private static IMutator CreateSwapMutator(Random random)
        {
            return new SwapMutator(random, MutationChance, GenotypeSize);
        }

        private static IMutator CreateInverseMutator(Random random)
        {
            return new InverseMutator(GenotypeSize, MutationChance, random);
        }

        private static IStatisticsCalculator CreateStatisticsCalculator()
        {
            return new StatisticsCalculator();
        }

        private static IGenotypeFactory CreateGenotypeFactory(Random random, IEvaluator evaluator)
        {
            return new GenotypeFactory(GenotypeSize, random, evaluator);
        }

        private static Func<int, List<Genotype>, bool> CreateIterationStopCondition()
        {
            return (iteration, population) => iteration >= Iterations;
        }

        private static Func<int, List<Genotype>, bool> CreateTimeStopCondition()
        {
            var endTime = DateTime.Now + WorkTime;
            return (iteration, population) => DateTime.Now >= endTime;
        }
    }
}
